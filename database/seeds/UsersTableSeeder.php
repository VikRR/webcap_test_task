<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userRole = \App\Models\Role::where('name', 'user')->first('id');
        $adminRole = \App\Models\Role::where('name', 'admin')->first('id');

        DB::table('users')->insert([
            'name' => 'user',
            'email' => 'user@test.com',
            'role_id' => $userRole->id,
            'password' => bcrypt('password')
        ]);

        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@test.com',
            'role_id' => $adminRole->id,
            'password' => bcrypt('password')
        ]);
    }
}
