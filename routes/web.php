<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'FeedbackController@create')->name('feedback.create');
Route::post('feedback', 'FeedbackController@store')->name('feedback.store');

Route::middleware(['auth'])->group(function(){
    Route::get('feedback-list', 'FeedbackController@index')->name('feedback.index');
});

Route::middleware(['admin'])->group(function(){
    Route::get('/admin', 'AdminController@index')->name('admin');
    Route::get('/users', 'UserController@index')->name('users');
});
