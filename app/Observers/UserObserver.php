<?php

namespace App\Observers;

use App\Models\Role;
use App\Models\User;

class UserObserver
{

    /**
     * Handle the user "saving" event.
     *
     * @param  User  $user
     *
     * @return void
     */
    public function saving(User $user)
    {
        $userRole = Role::userRole()->first();
        $user->role()->associate($userRole);
    }


}
