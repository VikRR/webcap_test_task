<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Guest extends Model
{
    protected $fillable = ['name', 'email', 'phone'];

    public function feedback()
    {
        return $this->morphMany(Feedback::class, 'feedbackable');
    }
}
