<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $fillable = ['feedback', 'img'];

    public function feedbackable()
    {
        return $this->morphTo();
    }
}
