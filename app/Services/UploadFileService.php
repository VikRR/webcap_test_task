<?php


namespace App\Services;


use Illuminate\Http\Request;

class UploadFileService
{

    public function uploadFile(Request $request, string $dir = 'images')
    {
        try {
            $imageName = time().'.'.$request->file->getClientOriginalExtension();
            $request->file->move(public_path($dir), $imageName);

            return $imageName;
        } catch (\Exception $e) {
            return false;
        }
    }
}