<?php


namespace App\Services;


use App\Models\Guest;
use App\Models\User;
use Illuminate\Http\Request;

class FeedbackService
{

    public function getFeedbackPerson(Request $request)
    {
        try {
            if ($request->has('user_id')) {
                return User::find($request->user_id);
            }
            $guestData['name'] = $request->name;
            if ($request->has('phone')) {
                $guestData['phone'] = $request->phone;
            }

            return Guest::firstOrCreate(
                ['email' => $request->email],
                $guestData
            );
        } catch (\Exception $e) {
            return false;
        }
    }

}