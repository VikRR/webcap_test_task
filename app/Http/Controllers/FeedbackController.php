<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateFeedbackRequest;
use App\Models\Feedback;
use App\Models\Guest;
use App\Models\User;
use App\Services\FeedbackService;
use App\Services\UploadFileService;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{

    protected $uploadService;
    protected $feedbackService;

    public function __construct(UploadFileService $upload_service, FeedbackService $feedback_service)
    {
        $this->uploadService   = $upload_service;
        $this->feedbackService = $feedback_service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
//        $feedback = Feedback::whereHasMorph('feedbackable', '*')->get();
        $feedback = Feedback::with('feedbackable')->get();

        return view('feedback.index', compact('feedback'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('feedback.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateFeedbackRequest  $request
     *
     * @return void
     */
    public function store(CreateFeedbackRequest $request)
    {
        // Find User Or Guest

        $person = $this->feedbackService->getFeedbackPerson($request);
        if ( ! $person) {
            session()->flash('error_msg', 'Something went wrong. Please try again later.');

            return back()->withInput();
        }
        /*if ($request->has('user_id')) {
            $user = User::find($request->user_id);
        } else {
            $guestData['name'] = $request->name;
            if ($request->has('phone')) {
                $guestData['phone'] = $request->phone;
            }

            $user = Guest::firstOrCreate(
                ['email' => $request->email],
                $guestData
            );
        }*/
        // Upload file
        $fileName = $this->uploadService->uploadFile($request);
        if ( ! $fileName) {
            session()->flash('error_msg', 'Could not load file. Please try again later.');

            return back()->withInput();
        }

        // Create Feedback
        $feedback = new Feedback;
        $feedback->fill([
            'feedback' => $request->feedback,
            'img'      => $fileName
        ]);
        // Associate feedback with user/guest
        $person->feedback()->save($feedback);

        return back();
    }

}
