<?php

namespace App\Http\Controllers;

use App\Models\User;

class UserController extends Controller
{
    public function index()
    {
        $users = User::isUser()->get(['id', 'name', 'email']);

        return view('user.index', compact('users'));
    }
}
