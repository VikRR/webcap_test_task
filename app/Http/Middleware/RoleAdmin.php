<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Closure;

class RoleAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ( ! $request->user() || ! $request->user()->hasRole('admin')) {
            return redirect()->route('feedback');
        }

        return $next($request);
    }
}
