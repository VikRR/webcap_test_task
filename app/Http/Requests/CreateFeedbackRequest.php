<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateFeedbackRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'     => 'bail|required|string|max:255',
            'email'    => 'required|email|max:255',
            'feedback' => 'required|string',
            'file'     => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ];
    }
}
