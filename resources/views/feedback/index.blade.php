@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @if(!empty($feedback))
                    @foreach($feedback as $message)
                        <div class="card">
                            <img class="card-img-top" style="height: 100px; width: 150px" src="{{asset("images/$message->img")}}" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title">{{$message->feedbackable->name}}</h5>
                                <span>email: {{$message->feedbackable->email}}</span>
                                @if(!empty($message->feedbackable->phone))
                                    <span>phone: {{$message->feedbackable->phone}}</span>
                                @endif
                                <p class="card-text">{{$message->feedback}}</p>
                            </div>
                        </div>
                    @endforeach
                @else
                    <h3>Comments not found.</h3>
                @endif
            </div>
        </div>
    </div>
@endsection
