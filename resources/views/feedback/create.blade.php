@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 offset-2">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (Session::has('error_msg'))
                    <div class="alert alert-danger" role="alert">
                        {{Session::get('error_msg')}}
                    </div>
                @endif
                <h2>Add feedback</h2>
                <form action="{{route('feedback.store')}}" method="POST" role="form"
                      enctype="multipart/form-data">
                    {{csrf_field()}}
                    @auth()
                        <input type="text" hidden value="{{Auth::user()->id}}" name="user_id">
                    @endauth
                    <div class="form-group">
                        <label for="uname">Name*:</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" id="uname"
                               placeholder="Enter Name" name="name" value="{{ old('name') }}" required>
                        @component('components.error-message', ['name'=>'name'])@endcomponent
                    </div>
                    <div class="form-group">
                        <label for="email">Email*:</label>
                        <input type="email" class="form-control @error('email') is-invalid @enderror" id="email"
                               placeholder="Enter Email" name="email" value="{{ old('email') }}" required>
                        @component('components.error-message', ['name'=>'email'])@endcomponent
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone:</label>
                        <input type="text" class="form-control @error('phone') is-invalid @enderror"
                               id="phone" placeholder="Enter Phone Number" value="{{ old('phone') }}"
                               name="phone">
                        @component('components.error-message', ['name'=>'phone'])@endcomponent
                    </div>
                    <div class="form-group">
                        <label for="feedback">Message*:</label>
                        <textarea class="form-control @error('feedback') is-invalid @enderror"
                                  id="feedback" placeholder="Enter Feedback" name="feedback"
                                  cols="30" rows="6" required>{{ old('feedback') }}</textarea>
                        @component('components.error-message', ['name'=>'feedback'])@endcomponent
                    </div>
                    <div class="form-group">
                        <label for="file">Add file*:</label>
                        <input type="file" class="form-control-file" id="file" name="file" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection
