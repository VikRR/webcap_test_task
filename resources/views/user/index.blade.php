@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @if(!empty($users))
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Login</th>
                            <th>Email</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <h3>Users not found.</h3>
                @endif
            </div>
        </div>
    </div>
@endsection
