<ul class="navbar-nav mr-auto">
    @guest
        @component('components.menu-item', ['route'=>'/', 'routeName'=>'feedback.create', 'item'=>'Feedback'])
        @endcomponent
    @else
        @component('components.menu-item', ['route'=>'/', 'routeName'=>'feedback.create', 'item'=>'Feedback'])
        @endcomponent
        @component('components.menu-item', ['route'=>'feedback-list', 'routeName'=>'feedback.index', 'item'=>'Feedback List'])
        @endcomponent
        @if(\Auth::user()->isAdmin())
            @component('components.menu-item', ['route'=>'users', 'routeName'=>'users', 'item'=>'Users'])
            @endcomponent
            @component('components.menu-item', ['route'=>'admin', 'routeName'=>'admin', 'item'=>'Admin'])
            @endcomponent
        @endif
    @endguest
</ul>